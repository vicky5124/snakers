use std::io::{Write, stdout};
use std::time::Duration;
use crossterm::{queue, cursor, terminal};
use crossterm::event::{poll, read, Event};
use rand::Rng;

fn generate_apple(len_x: u16, len_y: u16) -> [u16; 2] {
    let rand_x = rand::thread_rng().gen_range(1, len_y);
    let rand_y = rand::thread_rng().gen_range(0, len_x);

    let apple = [rand_x, rand_y];
    apple
}

fn main() -> crossterm::Result<()> {
    let size = terminal::size();
    let term_size;
    let mut key = crossterm::event::KeyCode::Right;
    let mut prev_key = key;
    let mut has_eaten = false;

    match size {
        Ok(x) => term_size = x,
        Err(e) => {
            println!("{}", e);
            std::process::exit(1);
        },
    }
    
    let len_x = term_size.0;
    let len_y = term_size.1;

    let mut apple = generate_apple(len_x, len_y);

    let mut y = 1;
    let mut x = 1;

    let mut y_line = Vec::new();
    let mut x_line = Vec::new();

    for _ in 0..len_y {
        for _ in 0..len_x {
            x_line.push(" ");
        }
        y_line.push(x_line);
        x_line = Vec::new();
    }

    let mut snake_vec = vec![[1,1]; 9];

    'outer: loop{
        let mut stdout = stdout();
        queue!(stdout, cursor::MoveTo(0, 0))?;

        for snake_block in snake_vec.clone().into_iter() {
            y_line[snake_block[0]][snake_block[1]] = "█";

            if snake_block[0] == apple[0] as usize && snake_block[1] == apple[1] as usize {
                has_eaten = true;
                'inner: loop {
                    apple = generate_apple(len_x, len_y);
                    if snake_block[0] == apple[0] as usize && snake_block[1] == apple[1] as usize {
                        apple = generate_apple(len_x, len_y);
                        break 'inner;
                    } else {
                        break 'inner;
                    }

                }
            } else {
                has_eaten = false;
            }
        }
        y_line[apple[0] as usize][apple[1] as usize] = "█";

        for y in &y_line {
            for x in y {
                print!("{}", x);
            }
            println!("");
        }

        terminal::enable_raw_mode()?;
        if poll(Duration::from_millis(250))? {
            match read()? {
                Event::Key(event) => key = event.code,
                _ => continue,
            }

            if key == crossterm::event::KeyCode::Right {
                y += 1;
                prev_key = key;
            } else if key == crossterm::event::KeyCode::Left {
                y -= 1;
                prev_key = key;
            } else if key == crossterm::event::KeyCode::Up {
                x -= 1;
                prev_key = key;
            } else if key == crossterm::event::KeyCode::Down {
                x += 1;
                prev_key = key;
            } else if key == crossterm::event::KeyCode::Char('q') {
                terminal::disable_raw_mode()?;
                std::process::exit(1);
            }
        } else {
            if prev_key == crossterm::event::KeyCode::Right {
                y += 1;
            } else if prev_key == crossterm::event::KeyCode::Left {
                y -= 1;
            } else if prev_key == crossterm::event::KeyCode::Up {
                x -= 1;
            } else if prev_key == crossterm::event::KeyCode::Down {
                x += 1;
            }
        }
        terminal::disable_raw_mode()?;
        
        if has_eaten == true {
            y_line[snake_vec[0][0]][snake_vec[0][1]] = " ";
            snake_vec.push([x as usize, y as usize]);
        } else {
            y_line[snake_vec[0][0]][snake_vec[0][1]] = " ";
            snake_vec.remove(0);
            for i in snake_vec.clone().into_iter() {
                if i == [x as usize, y as usize] {
                    std::process::exit(1);
                }
            }
            snake_vec.push([x as usize, y as usize]);

        }

        stdout.flush()?;
    }
}
